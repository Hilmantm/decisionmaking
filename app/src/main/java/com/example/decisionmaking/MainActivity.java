package com.example.decisionmaking;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText txtNIS, txtNamaSiswa, txtKelas, txtInputNilai;
    TextView txtTampilNIS, txtTampilNama, txttampilKelas, txtTampilNilai, txtTampilIndex, titleIndexNilai, titleNama;
    Button btnTampilkan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents() {
        txtNIS = findViewById(R.id.txtInputNIM);
        txtNamaSiswa = findViewById(R.id.txtInputNama);
        txtKelas = findViewById(R.id.txtInputKelas);

        txtInputNilai = findViewById(R.id.txtInputNilai);
        txtTampilNilai = findViewById(R.id.txtTampilNilai);
        txtTampilIndex = findViewById(R.id.txtTampilIndex);

        txtTampilNIS = findViewById(R.id.txtTampilNIS);
        txtTampilNama = findViewById(R.id.txtTampilNama);
        txttampilKelas = findViewById(R.id.txtTampilKelas);

        titleIndexNilai = findViewById(R.id.txtTitle);
        titleNama = findViewById(R.id.txtNama);

        btnTampilkan = findViewById(R.id.btn_tampilkan);
        btnTampilkan.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_tampilkan) {
            tampilkanData();
        }
    }

    private void tampilkanData() {
        String nis = txtNIS.getText().toString();
        String nama = txtNamaSiswa.getText().toString();
        String kelas = txtKelas.getText().toString();

        int nilai = (txtInputNilai.getText().toString().isEmpty()) ? 0 : Integer.parseInt(txtInputNilai.getText().toString());

        txtTampilNIS.setText("NIS : " + nis);
        txtTampilNama.setText("Nama : " + nama);
        titleNama.setText("Hilman Taris Muttaqin");
        txttampilKelas.setText("Kelas : " + kelas);
        txtTampilNilai.setText("Nilai : " + String.valueOf(nilai));
        txtTampilIndex.setText("Index : " + indexNilai(nilai));
    }

    private String indexNilai(int nilai) {
        String result;
        if(nilai >= 90) {
            result = "A";
        } else if(nilai >= 80 && nilai <= 89) {
            result = "B";
        } else if(nilai >= 70 && nilai <= 79) {
            result = "C";
        } else if(nilai >= 60 && nilai <= 69) {
            result = "D";
        } else {
            result = "E";
        }
        return result;
    }
}
